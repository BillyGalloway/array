﻿namespace Arrays_and_more_Arrays
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lboutput = new System.Windows.Forms.ListBox();
            this.btnArrayOne = new System.Windows.Forms.Button();
            this.btnArrayTwo = new System.Windows.Forms.Button();
            this.btnArrayThree = new System.Windows.Forms.Button();
            this.btnArrayFour = new System.Windows.Forms.Button();
            this.btnChangeArray = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIndexValue = new System.Windows.Forms.TextBox();
            this.txtElementValue = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnAverageOddElements = new System.Windows.Forms.Button();
            this.btnAverageNegElements = new System.Windows.Forms.Button();
            this.btnAverageElements = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnIndexSwap = new System.Windows.Forms.Button();
            this.txtIndexSwap = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtArrayNum = new System.Windows.Forms.TextBox();
            this.btnSelectArray = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.rtbUpdatedOutput = new System.Windows.Forms.RichTextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtIndexSwap2 = new System.Windows.Forms.TextBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // lboutput
            // 
            this.lboutput.FormattingEnabled = true;
            this.lboutput.Location = new System.Drawing.Point(6, 23);
            this.lboutput.Name = "lboutput";
            this.lboutput.Size = new System.Drawing.Size(116, 186);
            this.lboutput.TabIndex = 0;
            // 
            // btnArrayOne
            // 
            this.btnArrayOne.BackColor = System.Drawing.Color.Gray;
            this.btnArrayOne.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnArrayOne.Location = new System.Drawing.Point(237, 21);
            this.btnArrayOne.Name = "btnArrayOne";
            this.btnArrayOne.Size = new System.Drawing.Size(68, 30);
            this.btnArrayOne.TabIndex = 1;
            this.btnArrayOne.Text = "Array One";
            this.btnArrayOne.UseVisualStyleBackColor = false;
            this.btnArrayOne.Click += new System.EventHandler(this.btnEnter_Click);
            // 
            // btnArrayTwo
            // 
            this.btnArrayTwo.BackColor = System.Drawing.Color.Gray;
            this.btnArrayTwo.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnArrayTwo.Location = new System.Drawing.Point(237, 57);
            this.btnArrayTwo.Name = "btnArrayTwo";
            this.btnArrayTwo.Size = new System.Drawing.Size(68, 32);
            this.btnArrayTwo.TabIndex = 3;
            this.btnArrayTwo.Text = "Array Two";
            this.btnArrayTwo.UseVisualStyleBackColor = false;
            this.btnArrayTwo.Click += new System.EventHandler(this.btnArrayTwo_Click);
            // 
            // btnArrayThree
            // 
            this.btnArrayThree.BackColor = System.Drawing.Color.Gray;
            this.btnArrayThree.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnArrayThree.Location = new System.Drawing.Point(311, 21);
            this.btnArrayThree.Name = "btnArrayThree";
            this.btnArrayThree.Size = new System.Drawing.Size(80, 30);
            this.btnArrayThree.TabIndex = 4;
            this.btnArrayThree.Text = "Array Three";
            this.btnArrayThree.UseVisualStyleBackColor = false;
            this.btnArrayThree.Click += new System.EventHandler(this.btnArrayThree_Click);
            // 
            // btnArrayFour
            // 
            this.btnArrayFour.BackColor = System.Drawing.Color.Gray;
            this.btnArrayFour.FlatAppearance.BorderSize = 0;
            this.btnArrayFour.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnArrayFour.Location = new System.Drawing.Point(311, 56);
            this.btnArrayFour.Name = "btnArrayFour";
            this.btnArrayFour.Size = new System.Drawing.Size(80, 32);
            this.btnArrayFour.TabIndex = 5;
            this.btnArrayFour.Text = "Array Four";
            this.btnArrayFour.UseVisualStyleBackColor = false;
            this.btnArrayFour.Click += new System.EventHandler(this.btnArrayFour_Click);
            // 
            // btnChangeArray
            // 
            this.btnChangeArray.BackColor = System.Drawing.Color.Gray;
            this.btnChangeArray.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnChangeArray.Location = new System.Drawing.Point(5, 85);
            this.btnChangeArray.Name = "btnChangeArray";
            this.btnChangeArray.Size = new System.Drawing.Size(127, 23);
            this.btnChangeArray.TabIndex = 6;
            this.btnChangeArray.Text = "Enter";
            this.btnChangeArray.UseVisualStyleBackColor = false;
            this.btnChangeArray.Click += new System.EventHandler(this.btnChangeArray_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Index";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(4, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(49, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Element";
            // 
            // txtIndexValue
            // 
            this.txtIndexValue.Location = new System.Drawing.Point(56, 22);
            this.txtIndexValue.Name = "txtIndexValue";
            this.txtIndexValue.Size = new System.Drawing.Size(78, 22);
            this.txtIndexValue.TabIndex = 10;
            // 
            // txtElementValue
            // 
            this.txtElementValue.Location = new System.Drawing.Point(55, 53);
            this.txtElementValue.Name = "txtElementValue";
            this.txtElementValue.Size = new System.Drawing.Size(78, 22);
            this.txtElementValue.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.MenuText;
            this.groupBox1.Controls.Add(this.groupBox4);
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnArrayFour);
            this.groupBox1.Controls.Add(this.btnArrayThree);
            this.groupBox1.Controls.Add(this.btnArrayTwo);
            this.groupBox1.Controls.Add(this.btnArrayOne);
            this.groupBox1.Controls.Add(this.lboutput);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.MintCream;
            this.groupBox1.Location = new System.Drawing.Point(18, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(404, 215);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Array 1-4 by clicking or typing it in.";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.MenuText;
            this.groupBox2.Controls.Add(this.txtElementValue);
            this.groupBox2.Controls.Add(this.txtIndexValue);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.btnChangeArray);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.White;
            this.groupBox2.Location = new System.Drawing.Point(128, 95);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(139, 114);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Change any array";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.SystemColors.MenuText;
            this.groupBox3.Controls.Add(this.btnAverageOddElements);
            this.groupBox3.Controls.Add(this.btnAverageNegElements);
            this.groupBox3.Controls.Add(this.btnAverageElements);
            this.groupBox3.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.White;
            this.groupBox3.Location = new System.Drawing.Point(18, 233);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(181, 132);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Average array";
            // 
            // btnAverageOddElements
            // 
            this.btnAverageOddElements.BackColor = System.Drawing.Color.Gray;
            this.btnAverageOddElements.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAverageOddElements.Location = new System.Drawing.Point(6, 97);
            this.btnAverageOddElements.Name = "btnAverageOddElements";
            this.btnAverageOddElements.Size = new System.Drawing.Size(169, 29);
            this.btnAverageOddElements.TabIndex = 2;
            this.btnAverageOddElements.Text = "Average of Odd Elements";
            this.btnAverageOddElements.UseVisualStyleBackColor = false;
            this.btnAverageOddElements.Click += new System.EventHandler(this.btnAverageOddElements_Click);
            // 
            // btnAverageNegElements
            // 
            this.btnAverageNegElements.BackColor = System.Drawing.Color.Gray;
            this.btnAverageNegElements.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAverageNegElements.Location = new System.Drawing.Point(6, 56);
            this.btnAverageNegElements.Name = "btnAverageNegElements";
            this.btnAverageNegElements.Size = new System.Drawing.Size(169, 35);
            this.btnAverageNegElements.TabIndex = 1;
            this.btnAverageNegElements.Text = "Average of Negative Elements";
            this.btnAverageNegElements.UseVisualStyleBackColor = false;
            this.btnAverageNegElements.Click += new System.EventHandler(this.btnAverageNegElements_Click);
            // 
            // btnAverageElements
            // 
            this.btnAverageElements.BackColor = System.Drawing.Color.Gray;
            this.btnAverageElements.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAverageElements.Location = new System.Drawing.Point(6, 19);
            this.btnAverageElements.Name = "btnAverageElements";
            this.btnAverageElements.Size = new System.Drawing.Size(169, 32);
            this.btnAverageElements.TabIndex = 0;
            this.btnAverageElements.Text = "Average of all Elements";
            this.btnAverageElements.UseVisualStyleBackColor = false;
            this.btnAverageElements.Click += new System.EventHandler(this.btnAverageElements_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.SystemColors.MenuText;
            this.groupBox4.Controls.Add(this.txtIndexSwap2);
            this.groupBox4.Controls.Add(this.label4);
            this.groupBox4.Controls.Add(this.btnIndexSwap);
            this.groupBox4.Controls.Add(this.txtIndexSwap);
            this.groupBox4.Controls.Add(this.label1);
            this.groupBox4.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.White;
            this.groupBox4.Location = new System.Drawing.Point(273, 96);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(118, 113);
            this.groupBox4.TabIndex = 15;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Index Swap";
            // 
            // btnIndexSwap
            // 
            this.btnIndexSwap.BackColor = System.Drawing.Color.Gray;
            this.btnIndexSwap.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnIndexSwap.Location = new System.Drawing.Point(11, 84);
            this.btnIndexSwap.Name = "btnIndexSwap";
            this.btnIndexSwap.Size = new System.Drawing.Size(101, 23);
            this.btnIndexSwap.TabIndex = 2;
            this.btnIndexSwap.Text = "Enter";
            this.btnIndexSwap.UseVisualStyleBackColor = false;
            this.btnIndexSwap.Click += new System.EventHandler(this.btnIndexSwap_Click);
            // 
            // txtIndexSwap
            // 
            this.txtIndexSwap.Location = new System.Drawing.Point(50, 21);
            this.txtIndexSwap.Name = "txtIndexSwap";
            this.txtIndexSwap.Size = new System.Drawing.Size(50, 22);
            this.txtIndexSwap.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(36, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Index";
            // 
            // txtArrayNum
            // 
            this.txtArrayNum.Location = new System.Drawing.Point(5, 19);
            this.txtArrayNum.Name = "txtArrayNum";
            this.txtArrayNum.Size = new System.Drawing.Size(92, 22);
            this.txtArrayNum.TabIndex = 7;
            // 
            // btnSelectArray
            // 
            this.btnSelectArray.BackColor = System.Drawing.Color.Gray;
            this.btnSelectArray.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSelectArray.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnSelectArray.Location = new System.Drawing.Point(5, 45);
            this.btnSelectArray.Name = "btnSelectArray";
            this.btnSelectArray.Size = new System.Drawing.Size(92, 23);
            this.btnSelectArray.TabIndex = 8;
            this.btnSelectArray.Text = "Enter";
            this.btnSelectArray.UseVisualStyleBackColor = false;
            this.btnSelectArray.Click += new System.EventHandler(this.btnSelectArray_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.btnSelectArray);
            this.groupBox5.Controls.Add(this.txtArrayNum);
            this.groupBox5.ForeColor = System.Drawing.Color.White;
            this.groupBox5.Location = new System.Drawing.Point(128, 16);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(103, 73);
            this.groupBox5.TabIndex = 9;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Array Number";
            // 
            // rtbUpdatedOutput
            // 
            this.rtbUpdatedOutput.Location = new System.Drawing.Point(7, 19);
            this.rtbUpdatedOutput.Name = "rtbUpdatedOutput";
            this.rtbUpdatedOutput.Size = new System.Drawing.Size(198, 105);
            this.rtbUpdatedOutput.TabIndex = 10;
            this.rtbUpdatedOutput.Text = "";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Index";
            // 
            // txtIndexSwap2
            // 
            this.txtIndexSwap2.Location = new System.Drawing.Point(50, 52);
            this.txtIndexSwap2.Name = "txtIndexSwap2";
            this.txtIndexSwap2.Size = new System.Drawing.Size(50, 22);
            this.txtIndexSwap2.TabIndex = 4;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rtbUpdatedOutput);
            this.groupBox6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.ForeColor = System.Drawing.Color.White;
            this.groupBox6.Location = new System.Drawing.Point(205, 233);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(217, 132);
            this.groupBox6.TabIndex = 12;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Updated Information";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.MenuText;
            this.ClientSize = new System.Drawing.Size(434, 376);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Arrays";
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox lboutput;
        private System.Windows.Forms.Button btnArrayOne;
        private System.Windows.Forms.Button btnArrayTwo;
        private System.Windows.Forms.Button btnArrayThree;
        private System.Windows.Forms.Button btnArrayFour;
        private System.Windows.Forms.Button btnChangeArray;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtIndexValue;
        private System.Windows.Forms.TextBox txtElementValue;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btnAverageElements;
        private System.Windows.Forms.Button btnAverageOddElements;
        private System.Windows.Forms.Button btnAverageNegElements;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnIndexSwap;
        private System.Windows.Forms.TextBox txtIndexSwap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button btnSelectArray;
        private System.Windows.Forms.TextBox txtArrayNum;
        private System.Windows.Forms.RichTextBox rtbUpdatedOutput;
        private System.Windows.Forms.TextBox txtIndexSwap2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox6;
    }
}

