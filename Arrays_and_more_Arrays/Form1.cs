﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Arrays_and_more_Arrays
{
    public partial class Form1 : Form
    {
        int[] ArrayOne = { 1, 2, 0, 4, 23, 33, 52 };
        int[] ArrayTwo = { -5, 3, 2, 56, 34, 75, -43 };
        int[] ArrayThree = { -6, -8, 45, 34, -455, 367, 13, 345, 663, -63 };
        string[] ArrayFour = { "Billy", "Gina", "Code", "String", "Words", "Poop" };
        int ArrayHolder;
        

        public Form1()
        {
            InitializeComponent();
            
        }
        //All the button click Events
        private void btnEnter_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();

            SelectArray(1);
            rtbUpdatedOutput.AppendText("ArrayOne was called.\n");
            
        }
        private void btnArrayTwo_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();
            SelectArray(2);
            rtbUpdatedOutput.AppendText("ArrayTwo was called.\n");
        }
        private void btnArrayThree_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();
            SelectArray(3);
            rtbUpdatedOutput.AppendText("ArrayThree was called.\n");
        }
        private void btnArrayFour_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();
            SelectArray(4);
            rtbUpdatedOutput.AppendText("ArrayFour was called.\n");
        }
        private void btnSelectArray_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            
                int a;
                if (int.TryParse(txtArrayNum.Text, out a))
                {
                    SelectArray(a);
                    string b = String.Empty;
                    switch (a)
                    {
                        case 1:
                            b = "One";
                            break;
                        case 2:
                            b = "Two";
                            break;
                        case 3:
                            b = "Three";
                            break;
                        case 4:
                            b = "Four";
                            break;
                    }

                    rtbUpdatedOutput.AppendText("Array" + b + " was called.\n");
                }
                else if (!int.TryParse(txtArrayNum.Text, out a))
                {
                    FormatException fe = new FormatException();
                    MessageBox.Show(fe.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                }
                
                
        }
        private void btnChangeArray_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();
            
            if(ArrayHolder != 0)
            {
                switch (ArrayHolder)
                {
                    case 1:
                        SwapMethod(ArrayOne);
                        break;
                    case 2:
                        SwapMethod(ArrayTwo);
                        break;
                    case 3:
                        SwapMethod(ArrayThree);
                        break;
                    case 4:
                        SwapMethod(ArrayFour);
                        break;
                    default:
                        break;
                }
            }
            
            rtbUpdatedOutput.AppendText("Element values changed.\n");
        }
        private void btnIndexSwap_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();
            if (ArrayHolder != 0)
            {
                switch (ArrayHolder)
                {
                    case 1:
                        FirstLastMethod(ArrayOne);
                        break;
                    case 2:
                        FirstLastMethod(ArrayTwo);
                        break;
                    case 3:
                        FirstLastMethod(ArrayThree);
                        break;
                    case 4:
                        FirstLastMethod(ArrayFour);
                        break;
                    default:
                        break;
                }
            }
       
            
            
            rtbUpdatedOutput.AppendText(ArrayHolder + "Array " + "Elements were swapped.");
        }
        private void btnAverageElements_Click(object sender, EventArgs e)
        {
            
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();
            int avg = 0;
            if (ArrayHolder != 0)
            {
                switch (ArrayHolder)
                {
                    case 1:
                        avg = ArrayElementAverage(ArrayOne);
                        break;
                    case 2:
                        avg = ArrayElementAverage(ArrayTwo);
                        break;
                    case 3:
                        avg = ArrayElementAverage(ArrayThree);
                        break;
                    case 4:
                        MessageBox.Show("Array four is a String Array Cannot Average.");
                        break;
                    default:
                        break;
                }
                lboutput.Items.Add("Average: " + avg);
                rtbUpdatedOutput.AppendText("Array " + ArrayHolder + " was totalled and averaged.\n");
            }
            else
                lboutput.Items.Add("No Array Selected.");
        }
        private void btnAverageNegElements_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();
            int avg = 0;
            if (ArrayHolder != 0)
            {
                switch (ArrayHolder)
                {
                    case 1:
                        avg = NegativeAverageMethod(ArrayOne);
                        break;
                    case 2:
                        avg = NegativeAverageMethod(ArrayTwo);
                        break;
                    case 3:
                        avg = NegativeAverageMethod(ArrayThree);
                        break;
                    case 4:
                        MessageBox.Show("Array four is a String Array Cannot Average.");
                        break;
                    default:
                        break;
                }
                lboutput.Items.Add("Average: " + avg);
                rtbUpdatedOutput.AppendText("Array " + "was totalled and averaged by Negative Elements.\n");
            }
            else
                lboutput.Items.Add("No Array Selected.");
        }
        private void btnAverageOddElements_Click(object sender, EventArgs e)
        {
            lboutput.Items.Clear();
            rtbUpdatedOutput.ScrollToCaret();
            int avg = 0;
            if (ArrayHolder != 0)
            {
                switch (ArrayHolder)
                {
                    case 1:
                        avg = Odd(ArrayOne);
                        break;
                    case 2:
                        avg = Odd(ArrayTwo);
                        break;
                    case 3:
                        avg = Odd(ArrayThree);
                        break;
                    case 4:
                        MessageBox.Show("Array four is a String Array Cannot Average.");
                        break;
                    default:
                        break;
                }
                lboutput.Items.Add("Average: " + avg);
                rtbUpdatedOutput.AppendText("Array " + "was totalled and averaged by Odd Elements.\n");
            }
            else
                lboutput.Items.Add("No Array Selected.");
        }
        //End Click Events
        
        //Methods
        private int SelectArray(int a)
        {
            
            switch (a)
                {
                    case 1:
                        Display(ArrayOne);
                        break;
                    case 2:
                        Display(ArrayTwo);
                        break;
                    case 3:
                        Display(ArrayThree);
                        break;
                    case 4:
                        Display(ArrayFour);
                        break;
                    default:
                        {
                            FormatException fe = new FormatException();
                            MessageBox.Show(fe.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        }
                        break;
                }


            ArrayHolder = a;
            return ArrayHolder;
        }
        private int NegativeAverageMethod(int[]b)
        {
            int total = 0;
            int count = 0;
            for (int i = 0; i < b.Length; i++)
            {
                if(b[i] < 0)
                total += b[i];
                count++;
            }

            int avg;

            return avg = total / count;

        }
        private int Odd(int[]b)
        {
            int total = 0;
            int count = 0;
            for (int i = 0; i < b.Length; i++)
            {
                if(b[i] % 2 == 1)
                total += b[i];
                count++;
            }

            int avg;

            return avg = total / count;
        }
        private int ArrayElementAverage(int[] b)
        {

            int total = 0;

            for (int i = 0; i < b.Length; i++)
            {
                total += b[i];
            }

            int avg;

            return avg = total / b.Length;

        }
        private void Display(int[] b)
        {
            int i = 0;

            while (i < b.Length)
            {
                //Takes any int[] array as input and displays it in this format
                //each loop iteration starting from zero.
                //displaying the index first represented by i in this case
                //and then the value stored in that index
                lboutput.Items.Add("Index " + i + " Value " + b[i]);
                i++;
            }

        }
        private void Display(string[] AnyArray)
        {
            for (int i = 0; i < AnyArray.Length; i++)
            {
                lboutput.Items.Add("Index " + i + " Value " + AnyArray[i]);
            }

        }
        private void SwapMethod(int[]c)
        {
            int a, b;
            int.TryParse(txtIndexValue.Text, out a);
            int.TryParse(txtElementValue.Text, out b);
            c[a] = b;
            lboutput.Items.Add("Changed from " + a + "to " + b);

        }
        private void FirstLastMethod(int[]c)
        {
            int a, b, Empty;
            int.TryParse(txtIndexSwap.Text, out a);
            int.TryParse(txtIndexSwap2.Text, out b);
            Empty = c[a];
            c[a] = c[b];
            c[b] = Empty;
            lboutput.Items.Add("Changed from " + c[a] + "to " + c[b]);





        }
        private void FirstLastMethod(string[] c)
        {
            int a, b; 
            string[] d = {"","", "", "", "", "", "", ""};
            int.TryParse(txtIndexSwap.Text, out a);
            int.TryParse(txtIndexSwap2.Text, out b);
            d[a] = c[a];
            c[a] = c[b];
            c[b] = d[a];
            lboutput.Items.Add("Changed from " + c[a] + "to " + c[b]);
        }
        private void SwapMethod(string[]c)
        {
            string b;
            int a;
            int.TryParse(txtIndexValue.Text, out a);
            b = txtElementValue.Text;
            c[a] = b;
            

        }

        
      

      
    }
}
